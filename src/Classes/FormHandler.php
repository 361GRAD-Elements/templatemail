<?php

/**
 * 361GRAD Templatemail
 *
 * @package   dse-templatemail
 * @author    Yuriy Davats  <me@bcat.eu>
 * @link      http://www.bcat.eu
 * @license   GNU
 */

namespace Dse\Templatemail\Classes;


use Contao\Email;
use Contao\FormFieldModel;
use Contao\Frontend;
use Contao\FrontendTemplate;
use Psr\Log\LogLevel;

class FormHandler extends Frontend
{
    /**
     * Logger service
     * @var object
     */
    private $logger;

    public function __construct()
    {
        parent::__construct();
        $this->logger = $this->getContainer()->get('logger');
    }

    public function logInfo($msg) {
        $this->logger->log(LogLevel::INFO, $msg, [__METHOD__]);
    }

    public function logError($msg) {
        $this->logger->log(LogLevel::ERROR, $msg, [__METHOD__]);
    }

    /**
     * @param $arrPost
     * @param $arrFormDCA
     * @param $arrFiles
     *
     * @return void
     */
    public function onProcess($arrPost, $arrFormDCA, $arrFiles) {
        // do nothing with forms not marked as template form
        if (empty($arrFormDCA['dse_templatemail'])) {
            return;
        }

        $formData = $this->prepareFormData($arrPost);
        $subj = $this->replaceInsertTags($arrFormDCA['dse_templatemail_subj']);

        $this->sendMail($formData, $arrFormDCA, $subj, $arrFiles);

        return;
    }

    /**
     * Check posted data for the needed fields, extend with defaults if needed.
     *
     * @param array $arrPost
     *
     * @return array
     */
    private function prepareFormData($arrPost) {
        foreach ($this->arrRequiredKeys as $key) {
            if (array_key_exists($key, $arrPost)) {
                continue;
            }

            // set a simple & stupid default
            $arrPost[$key] = $key;
        }

        // add extras
        $arrPost["env_ip"] = $this->Environment->get("remoteAddr");

        return $arrPost;
    }

    private function sendMail($data, $dca, $subj, $arrFiles) {
        $recipientMail = $dca['dse_templatemail_recipient'];
        $html = $this->getHtml($dca["dse_email_template"], $data);
        $replyTo = $this->getReplyTo($dca['dse_templatemail_reply_to'], $data);

        $email = new Email();
        $email->subject = $subj;
        $email->html = $html;

        foreach ($arrFiles as $file) {
            $email->attachFile($file["tmp_name"], $file["type"]);
        }

        if ($replyTo) {
            $email->replyTo($replyTo);
        }

        //$email->text = $text; no text part
        if ($email->sendTo($recipientMail)) {
            $this->logInfo("Contact mail to $recipientMail successfully sent");
        } else {
            $this->logError("Failed sending a mail to $recipientMail");
        }
    }

    private function getHtml($template, $formData) {
        $objTemplate = new FrontendTemplate($template);
        $objTemplate->setData($formData);

        return $this->replaceInsertTags($objTemplate->parse());
    }

    private function getReplyTo($id, $formData) {
        $recipientModel = FormFieldModel::findById($id);
        return ($recipientModel) ? $formData[$recipientModel->name] : null;
    }

}
