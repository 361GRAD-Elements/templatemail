<?php

/**
 * 361GRAD Templatemail
 *
 * @package   dse-templatemail
 * @author    Yuriy Davats  <me@bcat.eu>
 * @link      http://www.bcat.eu
 * @license   GNU
 */

//HOOKS
$GLOBALS['TL_HOOKS']['processFormData'][] = ['Dse\\Templatemail\\Classes\\FormHandler', 'onProcess'];
