<?php

$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_legend'] = "Template Email Einstellungen";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail']['0'] = "Formular mit Template Mails";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail']['1'] = "Das ist ein Kontaktformular mit Email Templates.";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_recipient']['0'] = "Empfänger Email";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_recipient']['1'] = "Email-Adresse für Benachrichtigungen.";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_reply_to']['0'] = "Reply-To Email Feld";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_reply_to']['1'] = "Das Feld mit Email-Adresse die für die Antwort gesetzt wird.";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_subj']['0'] = "Email Subject";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_subj']['1'] = "Subject der Email Nachricht.";
$GLOBALS['TL_LANG']['tl_form']['dse_email_template']['0'] = "Email Template";
$GLOBALS['TL_LANG']['tl_form']['dse_email_template']['1'] = "Email Template, hier kann man Feld Namen dieses Formulars als Variablen verwenden um die Nachricht zu personalisieren.";
