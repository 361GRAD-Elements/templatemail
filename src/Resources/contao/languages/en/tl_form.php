<?php

$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_legend'] = "Template Email settings";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail']['0'] = "Form with template emails";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail']['1'] = "It is a form that needs template emails.";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_recipient']['0'] = "Recipient email";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_recipient']['1'] = "Email address for notifications.";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_reply_to']['0'] = "Reply-To Email";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_reply_to']['1'] = "Field with the email address used for replies to this message.";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_subj']['0'] = "Email Subject";
$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_subj']['1'] = "Subject of the email message.";
$GLOBALS['TL_LANG']['tl_form']['dse_email_template']['0'] = "Email Template";
$GLOBALS['TL_LANG']['tl_form']['dse_email_template']['1'] = "Email Template of this form, you can use member variables with field names to personalize the message.";
