<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * @package   BcatImmo
 * @author    Yuriy Davats  <me@bcat.eu>
 * @link      http://www.bcat.eu
 * @license   GNU
 */

/**
 * Modify palettes
 */
$GLOBALS['TL_DCA']['tl_form']['palettes']['__selector__'][] = 'dse_templatemail';
$GLOBALS['TL_DCA']['tl_form']['palettes']['default'] = str_replace('{email_legend}', '{dse_templatemail_legend:hide},dse_templatemail;{email_legend}', $GLOBALS['TL_DCA']['tl_form']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_form']['subpalettes']['dse_templatemail'] = 'dse_templatemail_recipient,dse_templatemail_reply_to,dse_templatemail_subj,dse_email_template';

/**
 * Add fields to tl_form
 */

$GLOBALS['TL_DCA']['tl_form']['fields']['dse_templatemail'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_form']['dse_templatemail'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'checkbox',
    'eval' => array('submitOnChange' => true),
    'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_form']['fields']['dse_templatemail_recipient'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_recipient'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => true, 'maxlength' => 255, 'rgxp'=>'email', 'tl_class' => 'w50'),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_form']['fields']['dse_templatemail_reply_to'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_reply_to'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_form_dse_templatemail', 'getEmailFields'),
    'eval' => array('includeBlankOption' => true, 'mandatory' => false, 'tl_class' => 'w50'),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_form']['fields']['dse_templatemail_subj'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_form']['dse_templatemail_subj'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_form']['fields']['dse_email_template'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_form']['dse_email_template'],
    'default' => 'dsemail_default',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_form_dse_templatemail', 'getEmailTemplates'),
    'eval' => array('mandatory' => true, 'tl_class' => 'w50'),
    'sql' => "varchar(32) NOT NULL default ''"
);

use Contao\FormFieldModel;
use Contao\Backend;
use Contao\DataContainer;

class tl_form_dse_templatemail extends Backend {

    /**
     * Return all email templates as array
     *
     * @return array
     */
    public function getEmailTemplates() {
        return $this->getTemplateGroup('dsemail_');
    }

    /**
     * Get all email fields of the current form
     * @return array
     */
    public function getEmailFields(DataContainer $dc) {

        $data = array();

        $allFields = FormFieldModel::findPublishedByPid($dc->activeRecord->id);
        if (empty($allFields)) {
            return array();
        }

        while ($allFields->next()) {
            if ($allFields->rgxp === 'email') {
                $data[$allFields->id] = $allFields->name;
            }
        }

        return $data;
    }

}
