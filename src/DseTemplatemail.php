<?php

/**
 * 361GRAD Templatemail
 *
 * @package   dse-templatemail
 * @author    Yuriy Davats  <me@bcat.eu>
 * @link      http://www.bcat.eu
 * @license   GNU
 */

namespace Dse\Templatemail;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Templatemail.
 */
class DseTemplatemail extends Bundle
{
}
